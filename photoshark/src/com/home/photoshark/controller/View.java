package com.home.photoshark.controller;

import java.util.ResourceBundle;

public interface View {

	void updateViewForResourceBundle(ResourceBundle resourceBundle);

}
