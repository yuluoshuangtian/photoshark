package com.home.photoshark.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JProgressBar;

import com.home.photoshark.model.ImageFileModel;
import com.home.photoshark.model.PhotoTableModel;

public class ImageController {

	private int filePreCount;
	private List<ImageFileModel> imageFiles;
	private PhotoTableModel photoTable;
	private PhotoTableModel emptyModel;

	private JProgressBar progressbar;

	public ImageController() {
		this.imageFiles = new ArrayList<ImageFileModel>();
		this.photoTable = new PhotoTableModel();
		this.emptyModel = new PhotoTableModel();
	}

	/**
	 * Load the image file in the target folder into an photoTableModel.
	 * 
	 * @param folder
	 *          the selected directory
	 * @param fileFilter
	 * @return PhotoTableModel
	 */
	public PhotoTableModel load(File folder, ImageFileFilter fileFilter) {

		imageFiles.clear();

		loadFilesInFolder(folder, fileFilter);

		this.photoTable.setImageArray(imageFiles);
		this.photoTable.initialize(folder);

		return this.photoTable;
	}

	public int count(File folder, ImageFileFilter fileFilter) {
		this.filePreCount = 0;
		countFilesInFolder(folder, fileFilter);
		return this.filePreCount;
	}

	/**
	 * Recursively load files in the current folder tree
	 * 
	 * @param folder
	 * @param filter
	 */
	private void loadFilesInFolder(File folder, ImageFileFilter filter) {
		File[] files = folder.listFiles(filter);
		for (File f : files) {
			if (f.isFile()) {
				// populate ImageFileModel from File
				ImageFileModel image = new ImageFileModel();
				image.setName(f.getName());
				image.setPath(f.getPath());
				image.setSize("" + f.length() / 1024 + "KB");
				image.setLastModifiedTime(new Date(f.lastModified()));

				if (imageFiles.contains(image)) {
					image.setDuplicated(true);
				}

				imageFiles.add(image);
				// FIXME modify progress bar
				if (progressbar != null) {
					progressbar.setValue(imageFiles.size());
				}

			} else if (f.isDirectory()) {
				loadFilesInFolder(f, filter);
			} else {
				throw new RuntimeException("Is there something else???!!");
			}
		}
	}

	private void countFilesInFolder(File folder, ImageFileFilter filter) {
		File[] files = folder.listFiles(filter);
		for (File f : files) {
			if (f.isFile()) {
				filePreCount++;
			} else if (f.isDirectory()) {
				countFilesInFolder(f, filter);
			} else {
				throw new RuntimeException("Is there something else???!!");
			}
		}
	}

	public PhotoTableModel getPhotoTable() {
		return photoTable;
	}

	public PhotoTableModel getEmptyModel() {
		return this.emptyModel;

	}

	public void setProgressbar(JProgressBar progressbar) {
		this.progressbar = progressbar;
	}

	/**
	 * rename a file that is from selected row in the image table. If there are
	 * more than 1 selected row, issue an alert.
	 * 
	 * @param rowIndexInModel
	 * @return true if successfully renamed file, false otherwise
	 */
	public boolean renameFile(int rowIndexInModel, String newFileName) {
		ImageFileModel fileModel = imageFiles.get(rowIndexInModel);
		File file = new File(fileModel.getPath());
		File newFile = new File(file.getParent(), newFileName);
		return file.renameTo(newFile);
	}

	public boolean deleteFile(int rowIndexInModel) {
		ImageFileModel fileModel = imageFiles.get(rowIndexInModel);
		File file = new File(fileModel.getPath());
		return file.delete();
	}

	/*
	 * Remove image files marked with duplicated
	 */
	public List<String> removeDuplicated() {
		List<String> result = new ArrayList<String>();
		for (ImageFileModel imageFile : imageFiles) {
			if (imageFile.isDuplicated()) {
				File file = new File(imageFile.getPath());
				if(file.delete() == false){
					result.add(file.getAbsolutePath());
				}
			}
		}
		return result;
	}
}
