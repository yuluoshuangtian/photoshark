package com.home.photoshark.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import com.home.photoshark.model.ResourceBundleModel;

public class ResourceBundleController implements Observer, ActionListener {
	private ResourceBundleModel resourceBundleModel;
	private List<View> viewsToControl;

	public ResourceBundleController(ResourceBundleModel model) {
		this.resourceBundleModel = model;
		this.viewsToControl = new ArrayList<View>();
		this.resourceBundleModel.addObserver(this);
	}

	public void addViewToControl(View v) {
		viewsToControl.add(v);
	}

	@Override
	public void update(Observable o, Object arg) {
		for (View v : viewsToControl) {
			v.updateViewForResourceBundle(resourceBundleModel.getResourceBundle());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("menuItemChineseClicked")) {
			resourceBundleModel.updateResourceBundleBasedOnLocale(Locale.CHINA);
		}

		if (e.getActionCommand().equals("menuItemEnglishClicked")) {
			resourceBundleModel.updateResourceBundleBasedOnLocale(Locale.ENGLISH);
		}

	}

}
