package com.home.photoshark.controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFileChooser;

import com.home.photoshark.model.ToolsPanelModel;
import com.home.photoshark.view.ToolsPanelView;

public class ToolsPanelController {

	private ToolsPanelModel toolsPanelModel;
	private ToolsPanelView toolsPanelView;

	private JFileChooser fileChooser;

	public ToolsPanelController(ToolsPanelModel model, ToolsPanelView view) {
		this.setToolsPanelModel(model);
		this.setToolsPanelView(view);
		fileChooser = new JFileChooser();
		this.registerComponentListener();
	}

	public void registerComponentListener() {
		this.toolsPanelView.addSelectDirectoryButtonListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent arg0) {

				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnValue = fileChooser.showOpenDialog(toolsPanelView);
				// if (returnValue == JFileChooser.APPROVE_OPTION) {
				// File directorySelected = fileChooser.getSelectedFile();
				// if (directorySelected == null || !directorySelected.isDirectory()) {
				// throw new RuntimeException("No file selected!");
				// }
				//
				// mainViewTable.setModel(imageController.getEmptyModel());
				// loadedFolderPathLabel.setText(directorySelected.getAbsolutePath());
				//
				// // TODO: call load controller to load pictures in a separate
				// // thread
				//
				// // load the image data into table model
				// mainViewTable.setModel(imageController.load(directorySelected,
				// fileFilter));
				// totalImageNumberInFolderLabel.setText("Total image loaded: " +
				// imageController.getPhotoTable().getImageArray().size());
				// } else {
				// loadedFolderPathLabel.setText("No folder selected!");
				// }
			}
		});
	}

	public ToolsPanelModel getToolsPanelModel() {
		return toolsPanelModel;
	}

	public void setToolsPanelModel(ToolsPanelModel toolsPanelModel) {
		this.toolsPanelModel = toolsPanelModel;
	}

	public ToolsPanelView getToolsPanelView() {
		return toolsPanelView;
	}

	public void setToolsPanelView(ToolsPanelView toolsPanelView) {
		this.toolsPanelView = toolsPanelView;
	}

}
