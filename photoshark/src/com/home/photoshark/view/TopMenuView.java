package com.home.photoshark.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import com.home.photoshark.controller.ResourceBundleController;
import com.home.photoshark.controller.View;

public class TopMenuView extends JMenuBar implements View {

	private static final long serialVersionUID = 1L;

	private static String PREFERENCES_MENU_TEXT_KEY = "label.topmenu.preferencemenu.text";

	private JMenu preferencesMenu;
	private JMenu languageMenu;
	private JMenuItem mntmGlobalPreferences;
	private JMenu aboutMenu;
	private JMenuItem menuItemHelp;
	private JMenuItem menuItemAbout;

	private ResourceBundleController resourceBundleController;

	public TopMenuView(ResourceBundleController resourceBundleController) {
		this.resourceBundleController = resourceBundleController;

		preferencesMenu = new JMenu("Options");
		this.add(preferencesMenu);

		languageMenu = new JMenu("Language");

		JMenuItem menuItemEnglish = new JMenuItem("English");
		menuItemEnglish.setActionCommand("menuItemEnglishClicked");
		menuItemEnglish.addActionListener(this.resourceBundleController);
		languageMenu.add(menuItemEnglish);

		JMenuItem menuItemChinese = new JMenuItem("中文");
		menuItemChinese.setActionCommand("menuItemChineseClicked");
		menuItemChinese.addActionListener(this.resourceBundleController);

		languageMenu.add(menuItemChinese);

		preferencesMenu.add(languageMenu);

		mntmGlobalPreferences = new JMenuItem("Global Preferences");
		preferencesMenu.add(mntmGlobalPreferences);

		aboutMenu = new JMenu("About");
		this.add(aboutMenu);

		menuItemHelp = new JMenuItem("Help");
		aboutMenu.add(menuItemHelp);

		menuItemAbout = new JMenuItem("About");
		aboutMenu.add(menuItemAbout);

	}

	@Override
	public void updateViewForResourceBundle(ResourceBundle resourceBundle) {
		this.preferencesMenu.setText(resourceBundle.getString(TopMenuView.PREFERENCES_MENU_TEXT_KEY));
	}

}
