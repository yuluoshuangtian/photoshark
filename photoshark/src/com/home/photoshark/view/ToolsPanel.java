package com.home.photoshark.view;

import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

public class ToolsPanel extends JPanel
{

	private static final long serialVersionUID = 1L;

	private JButton selectDirectoryButton;

	public ToolsPanel()
	{
		this.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

		// TODO: Change to "add folder"
		selectDirectoryButton = new JButton();
	}

	public void setSelectDirectoryButtonText(String text)
	{
		selectDirectoryButton.setText(text);
	}

	public void addSelectDirectoryButtonListener(MouseListener listener)
	{
		selectDirectoryButton.addMouseListener(listener);
	}

}
