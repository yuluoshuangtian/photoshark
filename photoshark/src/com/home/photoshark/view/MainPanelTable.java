package com.home.photoshark.view;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.home.photoshark.model.PhotoTableModel;

public class MainPanelTable extends JTable {

  private static final long serialVersionUID = 1L;
	final TableCellRenderer cellRender = new ColorTableCellRenderer();

	@Override
	public TableCellRenderer getCellRenderer(int row, int column) {

		PhotoTableModel model = (PhotoTableModel) this.getModel();
		if (model.isRowFromDuplicatedImage(row)) {
			return cellRender;
		}
		// else...
		return super.getCellRenderer(row, column);
	}

}
