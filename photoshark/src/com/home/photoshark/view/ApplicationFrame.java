package com.home.photoshark.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import com.home.photoshark.controller.ResourceBundleController;
import com.home.photoshark.model.ResourceBundleModel;

public class ApplicationFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	private ToolsPanelView toolsPanel;
	private InfoPanelView infoPanel;
	private MainPanelView mainPanel;
	private TopMenuView topMenu;

	private ResourceBundleController resourceBundleController;
	
	public ApplicationFrame(String title, int width, int height) {
		this.setTitle(title);
		this.setSize(width, height);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(screenSize.width / 2 - width / 2, screenSize.height / 2 - height / 2);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.resourceBundleController = new ResourceBundleController(new ResourceBundleModel());
		
		toolsPanel = new ToolsPanelView();
		this.getContentPane().add(toolsPanel, BorderLayout.NORTH);

		infoPanel = new InfoPanelView();
		this.getContentPane().add(infoPanel, BorderLayout.SOUTH);

		mainPanel = new MainPanelView();
		this.getContentPane().add(mainPanel, BorderLayout.CENTER);

		topMenu = new TopMenuView(resourceBundleController);
		this.setJMenuBar(topMenu);
		this.resourceBundleController.addViewToControl(topMenu);

	}
}
