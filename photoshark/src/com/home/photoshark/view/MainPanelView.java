package com.home.photoshark.view;

import javax.swing.JScrollPane;

public class MainPanelView extends JScrollPane {
  private static final long serialVersionUID = 1L;
	private MainPanelTable mainViewTable;

	public MainPanelView() {
		super(new MainPanelTable());
	}

	public MainPanelTable getMainViewTable() {
		return mainViewTable;
	}

	public void setMainViewTable(MainPanelTable mainViewTable) {
		this.mainViewTable = mainViewTable;
	}

}
