package com.home.photoshark.view;

import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

public class ToolsPanelView extends JPanel {

	private static final long serialVersionUID = 1L;

	private JButton selectDirectoryButton;
	private JButton btnReload;
	private JButton btnRename;
	private JButton btnDelete;
	private JButton btnPreview;
	private JButton btnDeleteAll;
	private JButton btnOpenSelectedFolder;

	public ToolsPanelView() {
		this.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

		// TODO: Change to "add folder"
		selectDirectoryButton = new JButton("TODO: text from resource bundle");
		this.add(selectDirectoryButton);

		btnReload = new JButton("重新扫描");
		this.add(btnReload);

		btnRename = new JButton("Rename");
		this.add(btnRename);

		btnDelete = new JButton("Delete");
		this.add(btnDelete);

		btnPreview = new JButton("预览");
		this.add(btnPreview);

		btnDeleteAll = new JButton("删除重复文件");
		this.add(btnDeleteAll);

		btnOpenSelectedFolder = new JButton("Open Selected Folder");
		this.add(btnOpenSelectedFolder);
	}

	public void setSelectDirectoryButtonText(String text) {
		selectDirectoryButton.setText(text);
	}

	public void addSelectDirectoryButtonListener(MouseListener listener) {
		selectDirectoryButton.addMouseListener(listener);
	}

}
