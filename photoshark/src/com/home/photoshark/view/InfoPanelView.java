package com.home.photoshark.view;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EtchedBorder;

public class InfoPanelView extends JPanel {
	private static final long serialVersionUID = 1L;
	private JPanel selectedFolderInfoPanel;
	private JProgressBar progressBar;
	private JLabel loadedFolderPathLabel;
	private JPanel totalImageInfoPanel;
	private JLabel totalImageNumberInFolderLabel;
	private JPanel infoProgressPanel;

	public InfoPanelView() {
		this.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		FlowLayout flowLayout = (FlowLayout) this.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);

		selectedFolderInfoPanel = new JPanel();
		selectedFolderInfoPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		this.add(selectedFolderInfoPanel);

		loadedFolderPathLabel = new JLabel("No folder selected.");
		selectedFolderInfoPanel.add(loadedFolderPathLabel);
		loadedFolderPathLabel.setBorder(null);

		totalImageInfoPanel = new JPanel();
		totalImageInfoPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		this.add(totalImageInfoPanel);

		totalImageNumberInFolderLabel = new JLabel("Total image loaded:");
		totalImageInfoPanel.add(totalImageNumberInFolderLabel);

		infoProgressPanel = new JPanel();
		infoProgressPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		this.add(infoProgressPanel);

		progressBar = new JProgressBar(0);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		infoProgressPanel.add(progressBar);
	}

}
