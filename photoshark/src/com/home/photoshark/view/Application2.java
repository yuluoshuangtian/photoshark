package com.home.photoshark.view;

import javax.swing.UIManager;

import com.home.photoshark.util.SpalashScreen;

public class Application2 {

	public static void main(String[] args) {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			final SpalashScreen splashScreen = new SpalashScreen();
			splashScreen.showSplash(new Runnable() {
				public void run() {
					ApplicationFrame windowFrame = new ApplicationFrame("PhotoShark", 500, 600);
					splashScreen.setVisible(false);
					windowFrame.setVisible(true);
				}
			});
		} catch (Exception e) {
			System.err.println(e);
		}

	}

}
