package com.home.photoshark.view;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import com.home.photoshark.controller.ImageController;
import com.home.photoshark.controller.ImageFileFilter;
import com.home.photoshark.model.PhotoTableModel;
import com.home.photoshark.util.FileExtension;
import com.home.photoshark.util.SpalashScreen;

@Deprecated
public class Application
{

	private JFrame frame;
	private JFileChooser fileChooser;
	private JLabel loadedFolderPathLabel;
	private JTable mainViewTable;
	private JScrollPane mainViewScrollPanel;
	private JLabel totalImageNumberInFolderLabel;
	private JProgressBar progressBar = new JProgressBar();

	private File directorySelected;
	private Desktop desktop;

	private ImageController imageController;
	private ImageFileFilter fileFilter;

	private ResourceBundle resourceBundle;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			final SpalashScreen splashScreen = new SpalashScreen();
			splashScreen.showSplash(new Runnable()
			{
				public void run()
				{
					Application window = new Application();
					splashScreen.setVisible(false);
					window.frame.setVisible(true);
				}
			});
		} catch (Exception e)
		{
			System.err.println(e);
		}

	}

	/**
	 * Create the application.
	 */
	public Application()
	{
		imageController = new ImageController();

		List<String> allowedExtensions = new ArrayList<String>();
		allowedExtensions.add(FileExtension.JPG.name().toLowerCase());
		allowedExtensions.add(FileExtension.PNG.name().toLowerCase());
		fileFilter = new ImageFileFilter(allowedExtensions, true);

		// desktop to test if action is supported on current platform
		if (Desktop.isDesktopSupported())
		{
			desktop = Desktop.getDesktop();
		}

		resourceBundle = ResourceBundle.getBundle("i18n",Locale.US);
		
		initializeUI();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initializeUI()
	{

		frame = new JFrame();
		frame.setTitle("PhotoShark");
		
		frame.setSize(500, 500);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel editToolsPanel = new JPanel();
		editToolsPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		frame.getContentPane().add(editToolsPanel, BorderLayout.NORTH);

		//TODO: Change to "add folder"
		final JButton btnSelectDirectory = new JButton(resourceBundle.getString("label.selectFolder"));
		fileChooser = new JFileChooser();
		btnSelectDirectory.addMouseListener(new MouseAdapter()
		{

			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				// show open dialog in the center of mainViewTable
				int returnValue = fileChooser.showOpenDialog(mainViewTable);

				if (returnValue == JFileChooser.APPROVE_OPTION)
				{
					directorySelected = fileChooser.getSelectedFile();
					if (directorySelected == null || !directorySelected.isDirectory())
					{
						throw new RuntimeException("No file selected!");
					}

					mainViewTable.setModel(imageController.getEmptyModel());

					loadedFolderPathLabel.setText(directorySelected.getAbsolutePath());

					// TODO: call load controller to load pictures in a separate
					// thread

					progressBar.setMaximum(imageController.count(directorySelected, fileFilter) * 3);
					// load the image data into table model
					mainViewTable.setModel(imageController.load(directorySelected, fileFilter));
					progressBar.setValue(progressBar.getMaximum());
					totalImageNumberInFolderLabel.setText("Total image loaded: " + imageController.getPhotoTable().getImageArray().size());
				} else
				{
					loadedFolderPathLabel.setText("No folder selected!");
				}
			}
		});

		editToolsPanel.add(btnSelectDirectory);

		JButton btnReload = new JButton("重新扫描");
		btnReload.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{

				if (directorySelected == null || !directorySelected.isDirectory())
				{
					loadedFolderPathLabel.setText("Can not reload without folder!");
				} else
				{
					progressBar.setMaximum(imageController.count(directorySelected, fileFilter) * 3);
					// load the image data into table model
					mainViewTable.setModel(imageController.load(directorySelected, fileFilter));
					progressBar.setValue(progressBar.getMaximum());
					totalImageNumberInFolderLabel.setText("Total image loaded: " + imageController.getPhotoTable().getImageArray().size());
				}

			}
		});
		editToolsPanel.add(btnReload);

		JButton btnRename = new JButton("Rename");
		btnRename.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				if (mainViewTable.getSelectedRowCount() != 1)
				{
					JOptionPane.showMessageDialog(frame, "Must select ONE file to rename.");
				} else
				{
					String newFileName = (String) JOptionPane.showInputDialog(frame, "input new file name with extension:", "Rename file", JOptionPane.PLAIN_MESSAGE);
					if (newFileName != null)
					{
						if (imageController.renameFile(mainViewTable.convertRowIndexToModel(mainViewTable.getSelectedRow()), newFileName) == false)
						{
							JOptionPane.showMessageDialog(frame, "failed to rename file");
						}
					}

				}

			}
		});
		editToolsPanel.add(btnRename);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				if (mainViewTable.getSelectedRowCount() != 1)
				{
					JOptionPane.showMessageDialog(frame, "Must select ONE file to delete.");
				} else
				{
					if (imageController.deleteFile(mainViewTable.convertRowIndexToModel(mainViewTable.getSelectedRow())) == false)
					{
						JOptionPane.showMessageDialog(frame, "failed to delete file");
					}

				}

			}
		});
		editToolsPanel.add(btnDelete);

		JButton btnPreview = new JButton("预览");
		btnPreview.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{

				if (mainViewTable.getSelectedRowCount() != 1)
				{
					JOptionPane.showMessageDialog(frame, "Must select ONE file to preview.", "Warning", JOptionPane.WARNING_MESSAGE);
				} else
				{
					// preview selected image
					JDialog popupDialog = new JDialog();
					File file = new File(((PhotoTableModel) mainViewTable.getModel()).getImageArray().get(mainViewTable.getSelectedRow()).getPath());
					BufferedImage image = null;
					try
					{
						image = ImageIO.read(file);
					} catch (IOException e1)
					{
						e1.printStackTrace();
					}

					JLabel imageLabel = new JLabel(new ImageIcon(image.getScaledInstance(mainViewScrollPanel.getSize().width, mainViewScrollPanel.getSize().height, Image.SCALE_DEFAULT)));
					popupDialog.getContentPane().add(imageLabel);
					popupDialog.setLocation(mainViewScrollPanel.getLocationOnScreen());
					popupDialog.setSize(mainViewScrollPanel.getSize());
					popupDialog.setAlwaysOnTop(true);
					popupDialog.setVisible(true);
				}
			}
		});

		JButton btnDeleteAll = new JButton("删除重复文件");

		btnDeleteAll.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				// TODO: remove duplicated file - based on name? - which one to
				// keep?

				String message = "Do you want to delete all following duplicated image files?(marked with yellow color)";
				if (JOptionPane.showConfirmDialog(frame, message, "Delete all duplicated?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
				{
					List<String> results = imageController.removeDuplicated();
					if (results.size() > 0)
					{
						System.err.println(results);

						StringBuilder errorMessage = new StringBuilder("The following file deletion has been failed:\n");
						for (String fileNameString : results)
						{
							errorMessage.append(fileNameString + "\n");
						}
						JOptionPane.showMessageDialog(frame, errorMessage, "file to delete", JOptionPane.ERROR_MESSAGE);
					}
				}

			}
		});
		editToolsPanel.add(btnDeleteAll);
		editToolsPanel.add(btnPreview);

		// open selected folder
		JButton btnOpenSelectedFolder = new JButton("Open Selected Folder");
		btnOpenSelectedFolder.addMouseListener(new MouseAdapter()
		{

			@Override
			public void mouseClicked(MouseEvent arg0)
			{

				if (directorySelected != null)
				{
					try
					{
						if (desktop.isSupported(Desktop.Action.OPEN))
						{
							desktop.open(directorySelected);
						}
					} catch (IOException e)
					{
						JOptionPane.showMessageDialog(frame, "failed to open folder", "Error", JOptionPane.ERROR_MESSAGE);
					}
				} else
				{
					JOptionPane.showMessageDialog(frame, "No selected folder", "Warning", JOptionPane.WARNING_MESSAGE);
				}

			}
		});
		editToolsPanel.add(btnOpenSelectedFolder);

		JPanel infoPanel = new JPanel();
		infoPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		FlowLayout flowLayout = (FlowLayout) infoPanel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		frame.getContentPane().add(infoPanel, BorderLayout.SOUTH);

		JPanel selectedFolderInfoPanel = new JPanel();
		selectedFolderInfoPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		infoPanel.add(selectedFolderInfoPanel);

		loadedFolderPathLabel = new JLabel("No folder selected.");
		selectedFolderInfoPanel.add(loadedFolderPathLabel);
		loadedFolderPathLabel.setBorder(null);

		JPanel totalImageInfoPanel = new JPanel();
		totalImageInfoPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		infoPanel.add(totalImageInfoPanel);

		totalImageNumberInFolderLabel = new JLabel("Total image loaded:");
		totalImageInfoPanel.add(totalImageNumberInFolderLabel);

		JPanel infoProgressPanel = new JPanel();
		infoProgressPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		infoPanel.add(infoProgressPanel);

		progressBar = new JProgressBar(0);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		infoProgressPanel.add(progressBar);
		imageController.setProgressbar(progressBar);

		final TableCellRenderer cellRender = new ColorTableCellRenderer();
		mainViewTable = new JTable()
		{
			private static final long serialVersionUID = 1L;

			public TableCellRenderer getCellRenderer(int row, int column)
			{

				PhotoTableModel model = (PhotoTableModel) this.getModel();
				if (model.isRowFromDuplicatedImage(row))
				{
					return cellRender;
				}
				// else...
				return super.getCellRenderer(row, column);
			}
		};
		mainViewTable.setModel(new DefaultTableModel());
		// mainViewTable.setColumnSelectionAllowed(true);
		// mainViewTable.setCellSelectionEnabled(true);
		mainViewTable.setFillsViewportHeight(true);

		mainViewScrollPanel = new JScrollPane(mainViewTable);
		frame.getContentPane().add(mainViewScrollPanel, BorderLayout.CENTER);

		JMenuBar topMenuBar = new JMenuBar();
		frame.setJMenuBar(topMenuBar);

		JMenu preferencesMenu = new JMenu("选项");
		topMenuBar.add(preferencesMenu);

		JMenu languageMenu = new JMenu("Language");
		

		JMenuItem menuItemEnglish = new JMenuItem("English");
		menuItemEnglish.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				System.out.println("menuItemEnglish clicked");
			}
		});
		languageMenu.add(menuItemEnglish);

		JMenuItem menuItemChinese = new JMenuItem("中文");
		menuItemChinese.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				resourceBundle = ResourceBundle.getBundle("i18n",Locale.CHINA);
				ResourceBundle.clearCache();
				System.out.println(resourceBundle.getString("label.selectFolder"));
				btnSelectDirectory.setText(resourceBundle.getString("label.selectFolder"));
				
			}
		});
		languageMenu.add(menuItemChinese);

		preferencesMenu.add(languageMenu);
		
		
		JMenuItem mntmGlobalPreferences = new JMenuItem("Global Preferences");
		preferencesMenu.add(mntmGlobalPreferences);

		
		
		JMenu aboutMenu = new JMenu("About");
		topMenuBar.add(aboutMenu);

		JMenuItem menuItemHelp = new JMenuItem("Help");
		aboutMenu.add(menuItemHelp);

		JMenuItem menuItemAbout = new JMenuItem("About");
		aboutMenu.add(menuItemAbout);
	}
}
