package com.home.photoshark.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.Timer;
import javax.swing.border.EtchedBorder;

public class SpalashScreen extends JWindow
{
	private static final long serialVersionUID = 1L;
	private static JProgressBar progressBar = new JProgressBar();
	private static int count;
	private static Timer timer;
	private static int progressBarMasValue = 100;

	public void showSplash(Runnable callback)
	{
		JPanel contentPane = (JPanel) getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBorder(new EtchedBorder());
		panel.setBackground(Color.WHITE);

		ImageIcon splashImage = new ImageIcon("resource/image/splash.jpg");
		JLabel imageLabel = new JLabel(splashImage);
		JLabel copyrightLabel = new JLabel("Version 1.0 Copyright 2013 - 2014");
		panel.add(imageLabel);
		panel.add(copyrightLabel);
		contentPane.add(panel);

		progressBar.setMaximum(progressBarMasValue);
		contentPane.add(progressBar);

		int width = splashImage.getIconWidth();
		int height = splashImage.getIconHeight() + 35;
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screenSize.width - width) / 2;
		int y = (screenSize.height - height) / 2;
		setBounds(x, y, width, height);

		setVisible(true);
		loadProgressBar(callback);

	}

	private void loadProgressBar(final Runnable callback)
	{
		ActionListener actionListener = new ActionListener()
		{
			public void actionPerformed(ActionEvent evt)
			{
				count++;
				progressBar.setValue(count);
				if (count == progressBarMasValue)
				{
					// When progressBar reach max value
					EventQueue.invokeLater(callback);
					timer.stop();
				}

			}

		};
		timer = new Timer(10, actionListener);
		timer.start();
	}
}