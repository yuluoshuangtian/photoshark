package com.home.photoshark.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import com.home.photoshark.util.FileProperties;

/**
 * 
 * After create a PhotoTableModel must one first; 1.assign imageArray and
 * columnNameArray. 2.Initialise the model.
 * 
 * @author Administrator
 * 
 */
public class PhotoTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;

	private Object[][] tableData;
	private String[] columnNames;

	private List<ImageFileModel> imageArray;
	private List<String> columnNameArray;

	public PhotoTableModel() {
		super();
		imageArray = new ArrayList<ImageFileModel>();
		columnNameArray = new ArrayList<String>();
	}

	public void initialize(File folder) {
		// Initialise column names
		getColumnNameArray().clear();
		getColumnNameArray().add(FileProperties.FILE_PROPERTY_VARIABLE_NAME);
		getColumnNameArray().add(FileProperties.FILE_PROPERTY_VARIABLE_PATH);
		getColumnNameArray().add(FileProperties.FILE_PROPERTY_VARIABLE_SIZE);
		getColumnNameArray().add(FileProperties.FILE_PROPERTY_VARIABLE_MODIFIED_TIME);

		columnNames = new String[getColumnNameArray().size()];
		columnNames = getColumnNameArray().toArray(columnNames);

		// Initialise table data
		tableData = new Object[getImageArray().size()][getColumnNameArray().size()];
		for (int i = 0; i < getImageArray().size(); i++) {
			for (int j = 0; j < getColumnNameArray().size(); j++) {

				if (getColumnNameArray().get(j).equalsIgnoreCase(FileProperties.FILE_PROPERTY_VARIABLE_NAME)) {
					tableData[i][j] = this.imageArray.get(i).getName();
				} else if (getColumnNameArray().get(j).equalsIgnoreCase(FileProperties.FILE_PROPERTY_VARIABLE_PATH)) {

					String absoluatePath = this.imageArray.get(i).getPath();

					// debug
					System.out.println("absoluatePath: " + absoluatePath);
					System.out.println("folder.getAbsolutePath: " + folder.getAbsolutePath());

					tableData[i][j] = absoluatePath;
				} else if (getColumnNameArray().get(j).equalsIgnoreCase(FileProperties.FILE_PROPERTY_VARIABLE_SIZE)) {
					tableData[i][j] = "" + this.imageArray.get(i).getSize();
				} else if (getColumnNameArray().get(j).equalsIgnoreCase(FileProperties.FILE_PROPERTY_VARIABLE_MODIFIED_TIME)) {
					tableData[i][j] = this.imageArray.get(i).getLastModifiedTime().toString();
				} else {
					// TODO: get more file attributes
				}

			}
		}

		this.setDataVector(tableData, columnNames);
	}

	public List<ImageFileModel> getImageArray() {
		return imageArray;
	}

	public void setImageArray(List<ImageFileModel> imageArray) {
		this.imageArray = imageArray;
	}

	public List<String> getColumnNameArray() {
		return columnNameArray;
	}

	public void setColumnNameArray(List<String> columnNameArray) {
		this.columnNameArray = columnNameArray;
	}

	public boolean isRowFromDuplicatedImage(int row) {
		// from the row, get the imageFile
		ImageFileModel imageFile = this.imageArray.get(row);
		if (imageFile.isDuplicated()) {
			return true;
		}

		return false;
	}
}
