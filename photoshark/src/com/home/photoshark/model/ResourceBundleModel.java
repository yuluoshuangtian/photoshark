package com.home.photoshark.model;

import java.util.Locale;
import java.util.Observable;
import java.util.ResourceBundle;

public class ResourceBundleModel extends Observable {

	private ResourceBundle resourceBundle;

	public ResourceBundleModel() {
		this.resourceBundle = ResourceBundle.getBundle("i18n", Locale.ENGLISH);
	}

	public void updateResourceBundleBasedOnLocale(Locale locale) {
		this.resourceBundle = ResourceBundle.getBundle("i18n", locale);
		ResourceBundle.clearCache();
		setChanged();
		notifyObservers();
	}

	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}

}
